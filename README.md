Ansible Crash Course by Michael Crilly

If you've got servers to administrate users to manage and software to deploy, then you'll love Ansible.

This Crash Course is for anyone new to Ansible and the concept of Configuration As Code.

Over 3,000 students so far! :-)

https://www.thecloud.coach/courses/ansible-crash-course/